<?php

class Parser {
    /** @var string переменная для хранения HTML страницы */
    protected $strHTML; 

    /**
     * Простейший вариант загрузки страницы
     *
     * @param string $url передаем адрес страницы
     */    
    private function getHTMLByUrl($url)
    {
        try {
            $this->strHTML = file_get_contents($url);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }        
    }
    
    protected function doParse()
    {
        // по регулярному выражению ищем все HTML теги
        preg_match_all('/<([^\/!][a-z1-9]*)/i', $this->strHTML, $matches);
        // подсчитываем количество всех значений массива
        return array_count_values($matches[1]);
    }

    /**
     * Публичная функция для работы с классом
     *
     * @param string $url передаем адрес страницы
     */     
    public function Parse($url)
    {
        $this->getHTMLByUrl($url);
        return $this->doParse();
    }    
}




