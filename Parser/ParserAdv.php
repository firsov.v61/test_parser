<?php

/**
 * Для демонстрации ООП расширяем базовый класс
 */
class ParserAdv extends Parser {

    /**
     * Добавляем более продвинутый способ загрузки страниц через Curl
     *
     * @param string $url передаем адрес страницы
     */     
    private function getHTMLByUrlAdv($url)
    {
        try {
            $rCh = curl_init();
            curl_setopt($rCh, CURLOPT_URL, $url);
            curl_setopt($rCh, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($rCh, CURLOPT_FOLLOWLOCATION, true);
            
            curl_setopt($rCh, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($rCh, CURLOPT_SSL_VERIFYHOST,  2);

            $sContent = curl_exec($rCh);
            $arInfo = curl_getinfo($rCh);

            if ($sContent === false) {
                throw new Exception(curl_error($rCh));

            } else {
                $this->strHTML = $sContent;
            }        
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        } finally {
            curl_close($rCh); 
        }            
    } 

    /**
     * Перегружаем финкцию Parse()
     *
     * @param string $url передаем адрес страницы
     */       
    public function Parse($url)
    {
        $this->getHTMLByUrlAdv($url);
        return $this->doParse();
    }    
}




